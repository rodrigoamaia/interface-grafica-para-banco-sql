[FASE-1]

## Criando o banco de dados

    https://web.dio.me/course/armazenamento-de-dados-1/learning/1535430f-a8ae-4710-800d-2d732656ad59?back=/track/cloud-devops-experience-banco-carrefour&tab=undefined&moduleId=undefined

[FASE-2]

## Criando Interface Gráfica

    https://www.youtube.com/watch?v=10pujTAd13Y&list=PLifTOxQosMmPWaBAxjZtdg-yzjI7H_rFA&index=1

## Habilitando as montagens dos Volumes

    -> Criar um diretório. Ex: /Users/rodrigo.maia/Documents/Pessoal/volume-tabela-monitoracao
    -> Abrir o Docker Desktop
    -> Clicar em configurações no lado superior esquerdo
    -> clicar em Resources
    -> Clicar em File Sharing
    -> Clicar na (+) referente a opção /path/to/exported/directory
    -> Selecionar o diretório criado: /Users/rodrigo.maia/Documents/Pessoal/volume-tabela-monitoracao

## Link de ajuda para configuração do Volume

    # https://stackoverflow.com/questions/45122459/mounts-denied-the-paths-are-not-shared-from-os-x-and-are-not-known-to-docke

## Criando volume local com Volume-Bind

	# docker run -e MYSQL_ROOT_PASSWORD=bdsql --name bdcadastro -d -p 3306:3306 --volume=/Users/rodrigo.maia/Documents/Pessoal/volume-tabela-monitoracao:/var/lib/mysql mysql

## Caso retorne um erro informando que a porta 3306 já está sendo utilizada, executar uns dos comandos:

    # sudo pkill mysql
    # killall -9 node

## Verificando conteúdo criado no HOST

    # ls /Users/rodrigo.maia/Documents/Pessoal/volume-tabela-monitoracao

## Verificando conteúdo criado no CONTAINER, de fora do container

    # docker exec bdcadastro ls /var/lib/mysql

## Acessando o container para verificar o conteúdo criado

    # docker exec -it bdcadastro /bin/bash
    # cd var/lib/mysql
    # ls
    # exit

## [ERRO] Acessando o banco de fora do container, logando como root

	# mysql -u root -p --protocol=tcp --port=3306

## Acessando o banco de dentro do container, logando como root

	# docker exec -it bdcadastro /bin/bash
    # mysql -u root -p --protocol=tcp --port=3306
    # bdsql

## Visualizando a existência de bancos

	# show databases;

## Criando um banco e dados

	# CREATE DATABASE tabelaMonitoracao;

## Visualizando a existência de bancos

	# show databases;

## Acessando o banco

	# use tabelaMonitoracao;

## Criando uma tabela (Dois modos)

[MODO-1]
	'''
	# CREATE TABLE cadastro (
	   Id int,
	   Status varchar(20),
	   Solicitante varchar(15),
	   Incidente varchar(150)'
	   Descricao varchar(2000),
	   LinkSlack varchar(200),
       Observacoes varchar(15),
       Apoio varchar(10),
       );	
	'''
[MODO-2]
	# CREATE TABLE cadastro ( Id int, Status varchar(20), Solicitante varchar(15), Incidente varchar(150), Descricao varchar(2000), LinkSlack varchar(200), Observacoes varchar(15), Apoio varchar(10));

## Inserindo dados na tabela

	# INSERT INTO cadastro ( Id,  Status,  Solicitante,  Incidente, Descricao,  LinkSlack, Observacoes, Apoio)  VALUES ( 1, 'Finalizado', 'Lucas', 'Erro na Pipeline da app Theme Compliler', 'Devido a alteração na estrutura do terraform, a pipeline quebrou', 'https://globo.slack.com/archives/C033B2359K7/p1663968490674359', 'NOTE', ' ');

## Conferindo a tabela criada

	# select * from cadastro;